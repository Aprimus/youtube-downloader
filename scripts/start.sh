#!/usr/bin/env bash


cd /home/ubuntu/Desktop/YouTube-Downloader
source /home/ubuntu/Desktop/YouTube-Downloader/my_env/bin/activate

exec /home/ubuntu/Desktop/YouTube-Downloader/my_env/bin/gunicorn youtube_downloader.wsgi