from django.core.management.base import BaseCommand

import pafy
from ffmpy import FFmpeg

import os
import re


class Command(BaseCommand):
    help = 'Download an mp3 from YouTube'

    def add_arguments(self, parser):
        parser.add_argument('link', type=str)

    def handle(self, *args, **options):
        url = options['link']
        video = pafy.new(url)
        audio = video.getbestaudio()
        webm = audio.download()
        title = re.sub('[^0-9a-zA-Z]+', '', audio.title)
        ff = FFmpeg(
            inputs={webm: None},
            outputs={'{}.mp3'.format(title): None}
        )
        ff.run()
        print(ff, type(ff))
        os.remove(webm)
