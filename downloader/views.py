from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib import messages
from django.shortcuts import redirect

from . import forms
from . import models


def audio_form_upload(request):
    if request.method == 'POST':
        form = forms.AudioForm(request.POST)
        if form.is_valid():
            response_data = {}
            youtube_url = form.cleaned_data['youtube_url']
            try:
                audio = models.Audio()
                audio.youtube_url = youtube_url
                #audio.download()
                audio.convert()
                audio.save()
                response_data['title'] = audio.title
                response_data['id'] = audio.pk
            except ValueError:
                response_data['invalid_url'] = 'Invalid url'

            return JsonResponse(response_data)
    else:
        form = forms.AudioForm()

    return render(request, 'index.html', {'form': form})


def detail(request, pk):
    obj = models.Audio.objects.get(pk=pk)
    return redirect(obj)