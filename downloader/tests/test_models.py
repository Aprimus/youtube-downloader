from django.test import TestCase
from django.conf import settings

from unittest.mock import patch
import os


from downloader.models import Audio
from downloader.utils import PafyMock, SampleWaveGenerator


MEDIA_URL = settings.MEDIA_URL
FORMAT_OUT = settings.CONVERT_FORMAT


class AudioModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Audio.objects.create(title='Audio-title',
                             youtube_url='https://www.youtube.com/watch?v=LbMEvVeONg4&t=19s')

    def setUp(self):
        self.audio = Audio.objects.get(id=1)

    def check_model_field(self, field_name):
        label = self.audio._meta.get_field(field_name).verbose_name
        return label

    def test_title_label(self):
        title_label = self.check_model_field('title')
        self.assertEqual(title_label, 'title')

    def test_youtube_url_field_label(self):
        youtube_url_field = self.check_model_field('youtube_url')
        self.assertEqual(youtube_url_field, 'youtube url')

    def test_audio_stream_field_label(self):
        audio_stream_field = self.check_model_field('audio_stream')
        self.assertEqual(audio_stream_field, 'audio stream')

    @patch('downloader.models.pafy')
    def test_audio_model_method_download(self, mock_pafy):
        mock_pafy.new.side_effect = PafyMock
        self.assertEqual(self.audio.download(), 'test.webm')

    @patch('downloader.models.Audio.download')
    def test_audio_model_method_convert(self, mock_download):
        with SampleWaveGenerator() as sample_file:
            mock_download.return_value = sample_file.name
            self.audio.convert()
        file_path = '{base_url}{title}.{format_out}'.format(base_url=MEDIA_URL,
                                                            title=self.audio.title,
                                                            format_out=FORMAT_OUT)
        self.assertTrue(os.path.exists(file_path))
        os.remove(file_path)
