from django.test import TestCase
from django.urls import reverse
from django.test import Client

import json
from unittest.mock import patch

class AudioFormUploadViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'index.html')

    # Test Post
    def test_post_view_with_invalid_url(self):
        header = {'Content-Type': 'application/json'}
        data = {'youtube_url': 'fake'}
        resp = self.client.post('/', data=data, **header)
        self.assertEqual(resp.status_code, 200)
        resp_data = resp.json()['invalid_url']
        self.assertEqual(resp_data, 'Invalid url')

    @patch('downloader.models.Audio.convert')
    def test_post_view_with_valid_url(self, mock_convert):
        mock_convert.retun_value = 'test_data.mp3'
        header = {'Content-Type': 'application/json'}
        data = {'youtube_url': 'https://www.youtube.com/watch?v=e0roV0YWWwE'}
        resp = self.client.post('/', data=data, **header)
        self.assertEqual(resp.status_code, 200)
        title = resp.json()['title']
        self.assertEqual(title, '')