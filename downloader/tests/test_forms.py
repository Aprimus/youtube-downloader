from django.test import TestCase
from downloader.forms import AudioForm


class AudioFormTest(TestCase):

    # Valid form data
    def test_audio_form_valid_data(self):
        form = AudioForm(data={'youtube_url': 'http://youtubeurltest'})
        self.assertTrue(form.is_valid())

    # Invalid form data
    def test_audio_form_invalid_data(self):
        form = AudioForm(data={'youtube_url': ''})
        self.assertFalse(form.is_valid())
