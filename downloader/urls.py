from django.urls import path

from . import views

urlpatterns = [
    path('', views.audio_form_upload, name='index'),
    path('<int:pk>/', views.detail, name='song-detail'),
]