import math, wave, array


class SoundObject:
    def download(self, filepath):
        return 'test.webm'


class PafyMock:
    def __init__(self, url):
        self.url = url
        self.title = 'A sample title'
        self.object = SoundObject()

    def getbestaudio(self):
        return self.object

# Sample wav file generator


class SampleWaveGenerator:

    def wav_sample_generator(self):
        duration = 3 # seconds
        freq = 440 # of cycles per second (Hz) (frequency of the sine waves)
        volume = 100 # percent
        data = array.array('h') # signed short integer (-32768 to 32767) data
        sampleRate = 44100 # of samples per second (standard)
        numChan = 1 # of channels (1: mono, 2: stereo)
        dataSize = 2 # 2 bytes because of using signed short integers => bit depth = 16
        numSamplesPerCyc = int(sampleRate / freq)
        numSamples = sampleRate * duration
        for i in range(numSamples):
            sample = 32767 * float(volume) / 100
            sample *= math.sin(math.pi * 2 * (i % numSamplesPerCyc) / numSamplesPerCyc)
            data.append(int(sample))
        filename = 'test_sample.wav'
        f = wave.open(filename, 'w')
        f.setparams((numChan, dataSize, sampleRate, numSamples, "NONE", "Uncompressed"))
        f.writeframes(data.tostring())
        f.close()
        return filename

    def __enter__(self):
        self.open_file = open(self.wav_sample_generator())
        return self.open_file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.open_file.close()
