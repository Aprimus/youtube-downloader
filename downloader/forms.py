from django import forms
from .models import Audio


class AudioForm(forms.Form):
    youtube_url = forms.CharField(
        max_length=255,
        widget=forms.TextInput(
            attrs={'id': 'InputUrl',
                   'class': 'form-control',
                   'name' : 'youtube_url',
                   'aria-describedby': 'Terms',
                   'placeholder': 'Insert a valid youtube url'}
        )
    )
