# Generated by Django 2.0.1 on 2018-01-09 11:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('downloader', '0007_auto_20180109_0958'),
    ]

    operations = [
        migrations.AlterField(
            model_name='audio',
            name='audio_stream',
            field=models.FileField(max_length=255, upload_to=''),
        ),
    ]
