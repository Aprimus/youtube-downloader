from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings

import os
import re

import pafy
from ffmpy import FFmpeg

fs = FileSystemStorage(location='media', base_url='/')


class Audio(models.Model):
    title = models.CharField(max_length=255, blank=True)
    youtube_url = models.URLField(blank=True, null=True)
    audio_stream = models.FileField(max_length=255, storage=fs)

    def download(self):
        video = pafy.new(self.youtube_url)
        audio = video.getbestaudio()
        self.title = re.sub(r'[^0-9a-zA-Z]+', '_', str(video.title))  # normalize the video title
        webm = audio.download(filepath=fs.base_location)
        return webm

    def convert(self):
        format_out = settings.CONVERT_FORMAT
        media_url = settings.MEDIA_URL
        input_file = self.download()
        output_file = '{base_url}{title}.{format_out}'.format(base_url=media_url,
                                                              title=self.title,
                                                              format_out=format_out)
        ff = FFmpeg(
            inputs={input_file: None},
            outputs={output_file: ["-y"]}  # overwrite set to -y (yes)
        )
        ff.run()
        self.audio_stream = output_file
        os.remove(input_file)
        return output_file

    def get_absolute_url(self):
        return self.audio_stream.url
